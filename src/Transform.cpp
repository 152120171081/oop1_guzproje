/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar-09/12/2019 Pazartesi
**/
#include "Transform.h"
Transform::Transform(double ang[], double tr[])
{
	if (ang != NULL)
		setAngle(ang);
	if (tr != NULL)
		setTrans(tr);
	transMatrix = new Eigen::Matrix4d();
	transMatrix->setIdentity();

}
Transform::Transform(Eigen::Vector3d ang, Eigen::Vector3d tr)
{
	setAngle(ang);
	setTrans(tr);
	transMatrix = new Eigen::Matrix4d();
	transMatrix->setIdentity();

}


Transform::~Transform()
{
}

Eigen::Vector3d Transform::getTrans()
{
	return trans;
}

void Transform::setTrans(double tr[])
{
	if (tr == NULL)
		return;
	for (int i = 0; i < 3; i++)
	{
		trans(i) = tr[i];
	}
}
void Transform::setTrans(Eigen::Vector3d tr)
{
	for (int i = 0; i < 3; i++)
	{
		trans(i) = tr[i];
	}
}

Eigen::Vector3d Transform::getAngle()
{
	return angle;
}

void Transform::setAngle(double ang[])
{
	if (ang == NULL)
		return;
	for (int i = 0; i < 3; i++)
	{
		angle(i) = ang[i];
	}
}

void Transform::setAngle(Eigen::Vector3d ang)
{
	for (int i = 0; i < 3; i++)
	{
		angle(i) = ang[i];
	}
}

void Transform::doTransform(Point &p)
{
	double newxyzPoint[4] = { 0 };
	for (int i = 0; i < 4; i++)
	{
		newxyzPoint[i] += (*transMatrix).coeff(i, 0) *  p.getX() + (*transMatrix).coeff(i, 1) * p.getY() + (*transMatrix).coeff(i, 2) * p.getZ() + (*transMatrix).coeff(i, 3) * 1;
	}
	Point *nP = new Point(newxyzPoint[0], newxyzPoint[1], newxyzPoint[2]);
	p = *nP;
}

void Transform::doTransform(PointCloud &pc)
{
	Point *NewPoint = new Point[pc.getPointNumber()]();
	vector<Point*> temp = pc.getPoints();
	int counter = 0;
	for (int i = 0; i < pc.getPointNumber(); i++)
	{
		Point pp = *(temp[i]);
		doTransform(pp);
		NewPoint[counter].setX(pp.getX());
		NewPoint[counter].setY(pp.getY());
		NewPoint[counter++].setZ(pp.getZ());
	}
	PointCloud *nPC = new PointCloud(counter);
	for (int i = 0; i < counter; i++)
	{
		nPC->setPoint(&NewPoint[i]);
	}
	pc = *nPC;
}


void Transform::MultiMatrix(Eigen::Matrix4d &left, const double right[MAXSIZE][MAXSIZE], double *result)
{
	for (int i = 0; i < MAXSIZE; i++)
	{
		for (int j = 0; j < MAXSIZE; j++)
		{
			result[(i*MAXSIZE)+j] = 0;
			for (int k = 0; k < MAXSIZE; k++)
			{
				result[(i*MAXSIZE) + j] += left.coeff(i, k)* right[k][j];
			}
		}
	}
}

void Transform::setTranslation(double tr[])
{
	double translation[4][4] = { {1,0,0,tr[0]},
								 {0,1,0,tr[1]},
								 {0,0,1,tr[2]},
								 {0,0,0,1} };

	double transMatrix1[4][4]{ 0 };
	MultiMatrix(*transMatrix, translation, &transMatrix1[0][0]);	
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			(*transMatrix)(i,j) = transMatrix1[i][j];
		}
	}
}

void Transform::setTranslation(Eigen::Vector3d tr)
{
	Eigen::Matrix4d translation;
	translation << 1, 0, 0, tr[0],
				   0, 1, 0, tr[1],
				   0, 0, 1, tr[2],
				   0, 0, 0, 1;

	*transMatrix = *transMatrix * translation;
}

void Transform::setRotation(Eigen::Vector3d ang)
{
	Eigen::Matrix4d  Rotation;

	Rotation(0, 0) = (cos(ang[2] * (PI / 180.0)) * cos(ang[1] * (PI / 180.0)));
	Rotation(0, 1) = (cos(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0))) - (sin(ang[2] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0)));
	Rotation(0, 2) = (cos(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0))) + (sin(ang[2] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0)));
	Rotation(0, 3) = 0;

	Rotation(1, 0) = (sin(ang[2] * (PI / 180.0)) * cos(ang[1] * (PI / 180.0)));
	Rotation(1, 1) = (sin(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0))) + (cos(ang[2] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0)));
	Rotation(1, 2) = (sin(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0))) - (cos(ang[2] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0)));
	Rotation(1, 3) = 0;

	Rotation(2, 0) = (-sin(ang[1] * (PI / 180.0)));
	Rotation(2, 1) = (cos(ang[1] * (PI / 180.0))*sin(ang[0] * (PI / 180.0)));
	Rotation(2, 2) = (cos(ang[1] * (PI / 180.0))*cos(ang[0] * (PI / 180.0)));
	Rotation(2, 3) = 0;

	Rotation(3, 0) = 0;
	Rotation(3, 1) = 0;
	Rotation(3, 2) = 0;
	Rotation(3, 3) = 1;
	*transMatrix = *transMatrix * Rotation;
}

void Transform::setRotation(double ang[])
{
	double transMatrix1[4][4]{ 0 };
	double Rotation[4][4] = { 0 };

	Rotation[0][0] = (cos(ang[2] * (PI / 180.0)) * cos(ang[1] * (PI / 180.0)));
	Rotation[0][1] = (cos(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0))) - (sin(ang[2] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0)));
	Rotation[0][2] = (cos(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0))) + (sin(ang[2] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0)));
	Rotation[0][3] = 0;

	Rotation[1][0] = (sin(ang[2] * (PI / 180.0)) * cos(ang[1] * (PI / 180.0)));
	Rotation[1][1] = (sin(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0))) + (cos(ang[2] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0)));
	Rotation[1][2] = (sin(ang[2] * (PI / 180.0)) * sin(ang[1] * (PI / 180.0)) * cos(ang[0] * (PI / 180.0))) - (cos(ang[2] * (PI / 180.0)) * sin(ang[0] * (PI / 180.0)));
	Rotation[1][3] = 0;

	Rotation[2][0] = (-sin(ang[1] * (PI / 180.0)));
	Rotation[2][1] = (cos(ang[1] * (PI / 180.0))*sin(ang[0] * (PI / 180.0)));
	Rotation[2][2] = (cos(ang[1] * (PI / 180.0))*cos(ang[0] * (PI / 180.0)));
	Rotation[2][3] = 0;

	Rotation[3][0] = 0;
	Rotation[3][1] = 0;
	Rotation[3][2] = 0;
	Rotation[3][3] = 1;

	MultiMatrix(*transMatrix, Rotation, &transMatrix1[0][0]);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			(*transMatrix)(i,j) = transMatrix1[i][j];
		}
	}

}