#include <iostream>
#include "DepthCamera.h"
#include "RadiusOutlierFilter.h"
#include "PassThroughFilter.h"
#include "PointCloudRecorder.h"
#include "PointCloudInterface.h"
#include "PointCloudGenerator.h"
#include "FilterPipe.h"
using namespace std;

#define PI 180 /*degree value*/
int main()
{
	PointCloudInterface PCI;
		
	RadiusOutlierFilter ROF(25);

	/*Firt one for upper limi,t second one for lover limit*/
	double LX[2] = { 400, 0 };
	double LY[2] = { 400, 0 };
	double LZ[2] = { 45, -45 };
	PassThroughFilter PTF(LX, LY, LZ);

	FilterPipe FP;
	FP.addFilter(&ROF);
	FP.addFilter(&PTF);

	Eigen::Vector3d ang1 = { 0, 0, -PI / 2 }, trans1 = { 100, 500, 50 };
	PointCloudGenerator *PCG1 = new DepthCamera("Input File/camera1.txt", ang1, trans1);
	PCG1->setFilterPipe(&FP);	
	PCI.addGenerator(PCG1);

	double ang2[] = { 0, 0, PI / 2 }, trans2[] = { 550, 50, 50 };
	PointCloudGenerator *PCG2 = new DepthCamera("Input File/camera2.txt", ang2, trans2);
	PCG2->setFilterPipe(&FP);
	PCI.addGenerator(PCG2);

	PointCloudRecorder *PCR = new PointCloudRecorder("Output File/Output.txt");
	PCI.setRecorder(PCR);

	PCI.generate();
	PCI.record();
	return 0;
}