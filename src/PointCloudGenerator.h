#pragma once
/*
* @file PointCloudGenerator.h
* @author Sezer Demir DEDEK 152120171081
* @date 28/12/2019 Pazar
**/
#include "PointCloud.h"
#include "FilterPipe.h"
#include "Transform.h"
#ifndef POINTcLOUDgENERATOR_H
#define POINTcLOUDgENERATOR_H
class PointCloudGenerator
{
protected:
	Transform transform;
	FilterPipe *filterPipe;
public:
	/// <summary> Constructer with parameter.</summary>
	/// <param name="ang"> Double array for create private Transform object.</param>
	/// <param name="tr"> Double array for create private Transform object.</param>
	PointCloudGenerator(double ang[]=NULL, double tr[]=NULL);

	/// <summary> Constructer with parameter.</summary>
	/// <param name="ang"> Eigen::Vector3d for create private Transform object.</param>
	/// <param name="tr"> Eigen::Vector3d for create private Transform object.</param>
	PointCloudGenerator(Eigen::Vector3d ang, Eigen::Vector3d tr);

	/// <summary> Destructer.</summary>
	~PointCloudGenerator();

	/// <summary> To set private FilterPipe pointer</summary>
	/// <param name="fp">(FilterPipe pointer to set FilterPipe *filterPipe.</param>
	void setFilterPipe(FilterPipe *fp);
	/// <summary> Pure virtual function to capture the points and set in PointCloud then returns it.</summary>
	/// <returns> PointCloud.</returns>
	virtual PointCloud capture()=0;
	/// <summary> <para>Pure virtual function to capture the points and set in PointCloud.</para>
	/// <para> Then pass into filters set on new space with Transform then returns it.</para></summary>
	/// <returns> PointCloud.</returns>
	virtual PointCloud captureFor() = 0;
};

#endif // !POINTcLOUDgENERATOR_H