#include "PointCloudGenerator.h"

PointCloudGenerator::PointCloudGenerator(double ang[], double tr[]):transform(ang,tr)
{
	filterPipe = NULL;
}PointCloudGenerator::PointCloudGenerator(Eigen::Vector3d ang, Eigen::Vector3d tr):transform(ang,tr)
{
	filterPipe = NULL;
}


PointCloudGenerator::~PointCloudGenerator()
{
	delete filterPipe;
}

void PointCloudGenerator::setFilterPipe(FilterPipe* fp)
{
	if (fp == NULL)
		return;
	filterPipe = fp;
}