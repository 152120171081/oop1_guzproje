#include "DepthCamera.h"

//Kerem Ko�
//152120181089
//08:12.2019
//OOP-1 proje-1

using namespace std;

DepthCamera::DepthCamera(string fileName, double ang[], double tr[]):PointCloudGenerator(ang,tr){
	this->fileName = fileName;
}
DepthCamera::DepthCamera(string fileName, Eigen::Vector3d ang, Eigen::Vector3d tr):PointCloudGenerator(ang,tr){
	this->fileName = fileName;
}

void DepthCamera::setFileName(string fileName){
	this->fileName = fileName;
}
string DepthCamera::getFileName() const{
	return fileName;
}
PointCloud DepthCamera::capture() {

	vector<Point*> points;
	fstream file(fileName);
	string line;
	string line1;
	string read = "";
	int count;
	bool check = false;
	if (file.is_open())
	{
		while (getline(file,line))
		{
			stringstream geek(line);
			read = "";
			count = 0;
			geek << line;
			
			Point *temp = new Point();
			check = false;
			while (true)
			{
				if (line.size() == (read.size() - 1))
					break;
				getline(geek, line1, ' ');
				if (line1 != "") {
					stringstream geek1(line1);
					double tempValue;
					geek1 >> tempValue;
					if (count == 0)
						temp->setX(tempValue);
					else if (count == 1)
						temp->setY(tempValue);
					else if (count == 2) {
						temp->setZ(tempValue);
						check = true;
					}
					count++;
						
				}
				read += line1 + " ";
			}
			if (check)
				points.push_back(temp);
		}

	}
	PointCloud *pc = new PointCloud(points.size());
	for (int i = 0; i < points.size(); i++)
	{
		pc->setPoint(points[i]);
	}
	return *pc;
}


PointCloud DepthCamera::captureFor()
{
	vector<Point*> points;
	fstream file(fileName);
	string line;
	string line1;
	string read = "";
	int count;
	bool check = false;
	if (file.is_open())
	{
		while (getline(file, line))
		{
			stringstream geek(line);
			read = "";
			count = 0;
			geek << line;

			Point *temp = new Point();
			check = false;
			while (true)
			{
				if (line.size() == (read.size() - 1))
					break;
				getline(geek, line1, ' ');
				if (line1 != "") {
					stringstream geek1(line1);
					double tempValue;
					geek1 >> tempValue;
					if (count == 0)
						temp->setX(tempValue);
					else if (count == 1)
						temp->setY(tempValue);
					else if (count == 2) {
						temp->setZ(tempValue);
						check = true;
					}
					count++;

				}
				read += line1 + " ";
			}
			if (check)
				points.push_back(temp);
		}

	}
	PointCloud *pc = new PointCloud(points.size());
	for (int i = 0; i < points.size(); i++)
	{
		pc->setPoint(points[i]);
	}

	filterPipe->filterOut(*pc);
	transform.setTranslation(transform.getTrans());
	transform.setRotation(transform.getAngle());
	transform.doTransform(*pc);
	return *pc;
}
