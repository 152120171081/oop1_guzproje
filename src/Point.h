//Kerem Ko�
//152120181089
#include <iostream>
#include <math.h>

using namespace std;
#ifndef POINT_H
#define POINT_H

class Point
{
private:
	double x;
	double y;
	double z;
public:
	/// <summary> Default constructer and constructer with parematers.</summary>
	/// <param name="x">Double to set private x. Default x:0</param>
	/// <param name="y">Double to set private y. Default y:0</param>
	/// <param name="y">Double to set private z. Default z:0</param>
	Point(double x=0,double y=0,double z=0);

	/// <summary> To get x value.</summary>
	/// <returns>Double x value</returns>
	double getX() const;

	/// <summary> To get y value.</summary>
	/// <returns>Double y value</returns>
	double getY() const;

	/// <summary> To get z value.</summary>
	/// <returns>Double z value</returns>
	double getZ() const;

	/// <summary> To set x value.</summary>
	/// <param name="x">Double to set private x.</param>
	void setX(double x);

	/// <summary> To set y value.</summary>
	/// <param name="y">Double to set private y.</param>
	void setY(double y);

	/// <summary> To set z value.</summary>
	/// <param name="z">Double to set private z.</param>
	void setZ(double z);

	/// <summary> This function check the all private value to is them equal.</summary>
	/// <param name="&p">Point adress to get raght value of the operator=='s.</param>
	/// <returns> bool value, if all of them(x,y,z) equal is true.</returns>
	bool operator== (const Point &p);

	/// <summary> This function calculate the distance of the two points.</summary>
	/// <param name="&p">Point adress to get second Point.</param>
	/// <returns> Double result value.</returns>
	double distance(const Point &p) const;

};


#endif // !POINT_H