#pragma once
/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "PointCloudFiter.h"
#ifndef PASSTHROUGHFILTER_H
#define PASSTHROUGHFILTER_H

class PassThroughFilter :public PointCloudFiter
{
	double upperLimitX;
	double lowerLimitX;
	double upperLimitY;
	double lowerLimitY;
	double upperLimitZ;
	double lowerLimitZ;
public:
	/// <summary>Constructer with parameters.</summary>
	/// <param name="LimitsX">Double array to set private upperLimitX and lowerLimitX. It has to be first value for upper limit then second value for lower limit</param>
	/// <param name="LimitsY">Double array to set private upperLimitY and lowerLimitY. It has to be first value for upper limit then second value for lower limit</param>
	/// <param name="LimitsZ">Double array to set private upperLimitZ and lowerLimitZ. It has to be first value for upper limit then second value for lower limit</param>
	PassThroughFilter(double LimitsX[],	double LimitsY[], double LimitsZ[]);

	/// <summary>Destructer</summary>
	~PassThroughFilter();

	/// <summary>To get upperLimitX value.</summary>
	/// <returns> returns the upperLimitX value. </returns>
	double getUperLimitX() const;

	/// <summary>To set upperLimitX with new value.</summary>
	/// <param name="upperLimitX">Double value to set private upperLimitX</param>
	void setUperLimitX(double upperLimitX);

	/// <summary>To get upperLimitY value.</summary>
	/// <returns> returns the upperLimitY value. </returns>
	double getUperLimitY() const;

	/// <summary>To set upperLimitY with new value.</summary>
	/// <param name="upperLimitY">Double value to set private upperLimitY</param>
	void setUperLimitY(double upperLimitY);

	/// <summary>To get upperLimitZ value.</summary>
	/// <returns> returns the upperLimitZ value. </returns>
	double getUperLimitZ() const;

	/// <summary>To set upperLimitZ with new value.</summary>
	/// <param name="upperLimitZ">Double value to set private upperLimitZ</param>
	void setUperLimitZ(double upperLimitZ);

	/// <summary>To get lowerLimitX value.</summary>
	/// <returns> returns the lowerLimitX value. </returns>
	double getLowerLimitX() const;

	/// <summary>To set lowerLimitX with new value.</summary>
	/// <param name="lowerLimitX">Double value to set private lowerLimitX</param>
	void setLowerLimitX(double lowerLimitX);

	/// <summary>To get lowerLimitY value.</summary>
	/// <returns> returns the lowerLimitY value. </returns>
	double getLowerLimitY() const;

	/// <summary>To set lowerLimitY with new value.</summary>
	/// <param name="lowerLimitY">Double value to set private lowerLimitY</param>
	void setLowerLimitY(double lowerLimitY);

	/// <summary>To get LowerLimitZ value.</summary>
	/// <returns> returns the LowerLimitZ value. </returns>
	double getLowerLimitZ() const;

	/// <summary>To set lowerLimitZ with new value.</summary>
	/// <param name="lowerLimitZ">Double value to set private lowerLimitZ</param>
	void setLowerLimitZ(double lowerLimitZ);

	/// <summary>This function do the filter with private values and delete the unnecessary values in the PointCloud.</summary>
	/// <param name="&pc">PointCloud address to do filter.</param>
	void filter(PointCloud &pc) const;

};

#endif // !PASSTHROUGHFILTER_H
