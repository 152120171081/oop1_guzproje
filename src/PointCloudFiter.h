#pragma once
/*
* @file PointCloudFilter.h
* @author Sezer Demir DEDEK 152120171081
* @date 28/12/2019 Pazar
**/
#include"PointCloud.h"
#ifndef POINTCLOUDFILTER_H
#define POINTCLOUDFILTER_H


class PointCloudFiter
{
public:
	/// <summary> Default constructer.</summary>
	PointCloudFiter();
	/// <summary> Default destructer.</summary>
	~PointCloudFiter();
	/// <summary> Pure virtual function to pass into filter.</summary>
	/// <param name="pc"> PointCloud address to filter</param>
	virtual void filter(PointCloud &pc) const = 0;
};

#endif // !POINTCLOUDFILTER_H