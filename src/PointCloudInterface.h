#pragma once
/*
* @file PointCloudInterface.h
* @author Sezer Demir DEDEK 152120171081
* @date 28/12/2019 Pazar
**/
#ifndef POINTCLOUDINTERFACE_H
#define POINTCLOUDINTERFACE_H
#include "PointCloud.h"
#include "PointCloudGenerator.h"
#include "FilterPipe.h"
#include "PointCloudRecorder.h"
class PointCloudInterface
{
	PointCloud pointCloud;
	vector<PointCloudGenerator*> generators;
	PointCloudRecorder *recorder;
public:
	/// <summary> Default constructer.</summary>
	PointCloudInterface();

	/// <summary> Default destructer.</summary>
	~PointCloudInterface();

	/// <summary> To add new generator in vector that hold PointClouudGenerator pointer.</summary>
	/// <param name="PCG"> New PointClouudGenerator pointer.</summary>
	void addGenerator(PointCloudGenerator *PCG);

	/// <summary> To set PointCloudRecorder pointer.</summary>
	/// <param name="PCR"> New PointCloudRecorder pointer.</summary>
	void setRecorder(PointCloudRecorder *PCR);
	/// <summary> To generate new PointCloud.</summary>
	/// <returns> Bool value.</returns>
	bool generate();
	/// <summary> To save the PointCloud.</summary>
	/// <returns> Bool value.</returns>
	bool record();
};

#endif // !POINTCLOUDINTERFACE_H
