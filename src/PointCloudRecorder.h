#pragma once
#include "PointCloud.h"
#include <iostream>
#include<fstream>
#include<sstream>
#include<vector>

//Kerem Ko�
//152120181089
//08:12.2019
//OOP-1 proje-1

class PointCloudRecorder
{
private:
	string fileName;
public:
	/// <summary>constructer with a paremater. </summary>
	/// <param name="fileName"> This paremater take a string to set private fileName</param>
	PointCloudRecorder(string fileName);

	/// <summary> To get fileName. </summary>
	/// <returns> returns string the fileName</returns>
	string getFileName() const;

	/// <summray>This function set the fileName with a new value. </summary>
	/// <param name="fileName"> This paremater take a string to set private fileName</param>
	void setFileName(string fileName);

	/// <summray>This function save to the file from Pointcloud.</summary>
	/// <param name="&pc"> This paremater take addresses of the PointCloud to save to file.</param>
	/// <returns> returns the bool value. false: there is not any file as fileName.</returns>
	bool save(const PointCloud& pc);

};

