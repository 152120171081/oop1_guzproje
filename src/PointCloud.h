#pragma once
/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "Point.h"
#include <iostream>
#include <vector>
using namespace std;
#ifndef POINTCLOUD_H
#define POINTCLOUD_H
class PointCloud
{
	vector<Point*> Points;
	int MaxCountofPoints;
public:
	/// <summary> Default constructer and constructer with paremater.</summary>
	/// <param name="MaxCountofPoints"> This point take a int to set private MaxCountofPoints.</param>
	PointCloud(int MaxCountofPoints =0);

	/// <summary> Destructer.</summary>
	~PointCloud();

	/// <summary> This function get the PointNumber </summary>
	/// <returns> returns the PointNumber.</returns>
	int getPointNumber()const;

	/// <summary> This function get the Point pointer first of the Point arrays. </summary>
	/// <returns> returns the vector that hold Point pointer.</returns>
	vector<Point*> getPoints()const;

	/// <summary> This function set the Point to end of the array. </summary>
	/// <param name="&p"> This paremater take address of the point to set it in array.</param>
	void setPoint(Point* p);

	/// <summary> This function sum the PointCloud and set them in a new PointCloud then returns it. </summary>
	/// <param name="&pc"> This paremater take the address of the PointCloud from right value of operator+'s.</param>
	/// <returns> returns new PointCloud adress</returns>
	PointCloud& operator+(const PointCloud& pc);

	/// <summary> This function set the PointCloud with new PointCloud. As a copy constructer.</summary>
	/// <param name="&pc"> This paremater take the address of the PointCloud from right value of operator='s.</param>
	/// <returns> returns new PointCloud adress.</returns>
	PointCloud& operator=(const PointCloud& pc);
};

#endif // !POINTCLOUD_H

