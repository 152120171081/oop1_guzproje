#pragma once
/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "PointCloudFiter.h"
#ifndef RADIUSOUTLIERFILTER_H
#define RADIUSOUTLIERFILTER_H
class RadiusOutlierFilter:public PointCloudFiter
{
	double radius;
public:
	/// <summary> Constructer with a paremater</summary>
	/// <param name="radius"> This paremater take a double value to set private radius.</param>
	RadiusOutlierFilter(double radius);

	/// <summary> Destructer.</summary>
	~RadiusOutlierFilter();

	/// <summary> returns the private radius value.</summary>
	/// <returns> returns the double.</returns>
	double getRadius() const;

	/// <summary> This function set the radius with a new value.</summary>
	/// <param name="radius"> This paremater take a double value to set private radius.</param>
	void setRadius(double radius);

	/// <summary> This function filter the PointCloud with radius value.</summary>
	/// <param name="&pc"> This paremater take PointCloud addresses to do filter.</param>
	void filter(PointCloud& pc) const;
};


#endif // !RADIUSOUTLIERFILTER_H
