#pragma once
/*
* @file FilterPipe.h
* @author Sezer Demir DEDEK 152120171081
* @date 28/12/2019 Pazar
**/
#include "PointCloudFiter.h"
#include <vector>
#ifndef FILTERPIPE_H
#define FILTERPIPE_H
class FilterPipe
{
	vector<PointCloudFiter*> filters;
public:
	/// <summary>Default constructer. </summary>
	FilterPipe();

	/// <summary>Default destructer. </summary>
	~FilterPipe();

	/// <summary>To set new filter in the list. </summary>
	/// <param name="filter">PointCloudFiter pointer to set in filters. </param>
	void addFilter(PointCloudFiter *filter);

	/// <summary>To filter the PointCloud in filters. </summary>
	/// <param name="pc">PointCloud address to pass all filters. </param>
	void filterOut(PointCloud &pc);
};
#endif // !FILTERPIPE
