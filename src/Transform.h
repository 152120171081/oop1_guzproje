#pragma once
/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "PointCloud.h"
#include "eigen-eigen-323c052e1731/Eigen/Dense"
#ifndef TRANSFORM
#define TRANSFORM
#define PI 3.1415
#define MAXSIZE 4// for matrix row and column values

class Transform
{
	Eigen::Vector3d angle;
	Eigen::Vector3d trans;
	Eigen::Matrix4d *transMatrix;

	/// <summary> This function multiply the matrixes.</summary>
	/// <param name="left"> This paremater take a Eigen::Matrix4d to do multiply.</param>
	/// <param name="right"> This paremater take a matris to do multiply.</param>
	/// <param name="*result"> This paremater take a double pointer to save the result of the multiplying.</param>
	void MultiMatrix(Eigen::Matrix4d &left, const double right[MAXSIZE][MAXSIZE], double *result);

public:
	/// <summary> Constructer with parematers.</summary>
	/// <param name="ang"> This paremater take a array to set private Eigen::Vector3d angle.</param>
	/// <param name="tr"> This paremater take a array to set private Eigen::Vector3d trans.</param>
	Transform(double ang[], double tr[]);


	/// <summary> Constructer with parematers.</summary>
	/// <param name="ang"> This paremater take a Eigen::Vector3d  to set private Eigen::Vector3d angle.</param>
	/// <param name="tr"> This paremater take a Eigen::Vector3d  to set private Eigen::Vector3d trans.</param>
	Transform(Eigen::Vector3d ang, Eigen::Vector3d tr);

	/// <summary> Destructer.</summary>
	~Transform();

	/// <summary> To get Angle value.</summary>
	/// <returns> Eigen::Vector3d</returns>
	Eigen::Vector3d getAngle();

	/// <summary> This function set the Eigen::Vector3d angle with a new array.</summary>
	/// <param name="ang"> This paremater take a array to set private Eigen::Vector3d angle.</param>
	void setAngle(double ang[]);

	/// <summary> This function set the Eigen::Vector3d angle with a new Eigen::Vector3d .</summary>
	/// <param name="ang"> This paremater take a array to set private Eigen::Vector3d angle.</param>
	void setAngle(Eigen::Vector3d  ang);

	/// <summary> To get Trans value.</summary>
	/// <returns> Eigen::Vector3d</returns>
	Eigen::Vector3d getTrans();

	/// <summary> This function set the trans array with a new array.</summary>
	/// <param name="tr"> This paremater take a array to set private Eigen::Vector3d trans.</param>
	void setTrans(double tr[]);

	/// <summary> This function set the trans array with a new array.</summary>
	/// <param name="tr"> This paremater take a array to set private Eigen::Vector3d trans.</param>
	void setTrans(Eigen::Vector3d tr);

	/// <summary> To set transMatrix with transform valuess.</summary>
	/// <param name="tr"> Double array for transform values.</param>
	void setTranslation(double tr[]);

	/// <summary> To set transMatrix with transform valuess.</summary>
	/// <param name="tr"> Eigen::Vector3d for transform values.</param>
	void setTranslation(Eigen::Vector3d tr);

	/// <summary> To set transMatrix with rotation valuess.</summary>
	/// <param name="ang"> Eigen::Vector3d for rotation values.</param>
	void setRotation(Eigen::Vector3d ang);

	/// <summary> To set transMatrix with rotation valuess.</summary>
	/// <param name="ang"> Double array for rotation values.</param>
	void setRotation(double ang[]);

	/// <summary> This function set the point to new location on new space.</summary>
	/// <param name="&pc"> This paremater take a address of the Point.</param>
	void doTransform(Point &p);

	/// <summary> This function set the PointCloud to new location on new space.</summary>
	/// <param name="&pc"> This paremater take a address of the PointCloud.</param>
	void doTransform(PointCloud &pc);

};

#endif // !TRANSFORM
