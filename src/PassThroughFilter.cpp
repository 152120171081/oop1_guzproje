/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "PassThroughFilter.h"

PassThroughFilter::PassThroughFilter(double LimitsX[], double LimitsY[], double LimitsZ[])
{
	this->upperLimitX = LimitsX[0];
	this->upperLimitY = LimitsY[0];
	this->upperLimitZ = LimitsZ[0];
	this->lowerLimitX = LimitsX[1];
	this->lowerLimitY = LimitsY[1];
	this->lowerLimitZ = LimitsZ[1];
}


PassThroughFilter::~PassThroughFilter()
{
}

double PassThroughFilter::getUperLimitX() const
{	return upperLimitX; }

void PassThroughFilter::setUperLimitX(double upperLimitX)
{	this->upperLimitX = upperLimitX; }

double PassThroughFilter::getUperLimitY() const
{	return upperLimitY; }

void PassThroughFilter::setUperLimitY(double upperLimitY)
{	this->upperLimitY = upperLimitY; }

double PassThroughFilter::getUperLimitZ() const
{	return upperLimitZ; }

void PassThroughFilter::setUperLimitZ(double upperLimitZ)
{	this->upperLimitZ = upperLimitZ; }

double PassThroughFilter::getLowerLimitX() const
{	return lowerLimitX; }

void PassThroughFilter::setLowerLimitX(double lowerLimitX)
{	this->lowerLimitX = lowerLimitX; }

double PassThroughFilter::getLowerLimitY() const
{	return lowerLimitY; }

void PassThroughFilter::setLowerLimitY(double lowerLimitY)
{	this->lowerLimitY = lowerLimitY; }

double PassThroughFilter::getLowerLimitZ() const
{	return lowerLimitZ; }

void PassThroughFilter::setLowerLimitZ(double lowerLimitZ)
{	this->lowerLimitZ = lowerLimitZ; }

void PassThroughFilter::filter(PointCloud& pc) const
{
	int pointNumber = pc.getPointNumber();
	bool check = false;
	int counter = 0;
	Point *newPoint = new Point[pointNumber]();
	vector<Point*> temp = pc.getPoints();
	for (int i = 0; i < pointNumber; i++)
	{
		double x = temp[i]->getX();
		double y = temp[i]->getY();
		double z = temp[i]->getZ();
		bool tempx = (x >= lowerLimitX && x <= upperLimitX);
		bool tempy = (y >= lowerLimitY && y <= upperLimitY);
		bool tempz = (z >= lowerLimitZ && z <= upperLimitZ);
		if ( tempx&& tempy && tempz)
		{
			newPoint[counter].setX(x);
			newPoint[counter].setY(y);
			newPoint[counter].setZ(z);
			counter++;
		}
	}
	PointCloud *newPC = new PointCloud(counter);
	for (int i = 0; i < counter; i++)
	{
		Point * temp = &newPoint[i];
		newPC->setPoint(temp);
	}
	pc = *newPC;
}