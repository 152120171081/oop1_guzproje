#include "PointCloudInterface.h"

PointCloudInterface::PointCloudInterface()
{
	recorder = NULL;
}

PointCloudInterface::~PointCloudInterface()
{

}


void PointCloudInterface::addGenerator(PointCloudGenerator* PCG)
{
	if (PCG != NULL)
		generators.push_back(PCG);
}
void PointCloudInterface::setRecorder(PointCloudRecorder* PCR)
{
	if (PCR != NULL)
		recorder = PCR;
}
bool PointCloudInterface::generate()
{
	if (generators.size() == 0)
		return false;
	for (int i = 0; i < generators.size(); i++)
	{
		pointCloud = pointCloud + generators[i]->captureFor();
	}
	return true;
}
bool PointCloudInterface::record()
{
	if (recorder == NULL)
		return false;
	recorder->save(pointCloud);
	return true;
}