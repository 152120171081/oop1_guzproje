/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "PointCloud.h"

PointCloud::PointCloud(int MaxCountofPoints)
{
	this->MaxCountofPoints = MaxCountofPoints;
}


PointCloud::~PointCloud()
{
}


int PointCloud::getPointNumber() const
{
	return this->Points.size();
}

vector<Point*> PointCloud::getPoints() const 
{
	return this->Points;
}

void PointCloud::setPoint(Point* p)
{
	if (p == NULL)
		return;
	if (Points.size() < MaxCountofPoints)
	{
		Points.push_back(p);
	}
}

PointCloud& PointCloud::operator+(const PointCloud& pc)
{
	int PointNumberTemp = this->getPointNumber() + pc.getPointNumber();
	PointCloud *temp=new PointCloud(PointNumberTemp);
	temp->MaxCountofPoints = PointNumberTemp;
	for (int i = 0; i < PointNumberTemp; i++)
	{
		if (i < Points.size())
			temp->setPoint(this->Points[i]);
		else
			temp->setPoint(pc.Points[i - Points.size()]);
	}
	return *temp;
}
PointCloud& PointCloud::operator=(const PointCloud& pc)
{
	this->Points.clear();
	this->Points = pc.Points;
	return *this;
}