#include "FilterPipe.h"


FilterPipe::FilterPipe()
{

}

FilterPipe::~FilterPipe()
{

}

void FilterPipe::addFilter(PointCloudFiter *filter)
{
	if (filter == NULL)
		return;
	filters.push_back(filter);
}

void FilterPipe::filterOut(PointCloud &pc)
{
	for (int i = 0; i < filters.size(); i++)
	{
		filters[i]->filter(pc);
	}
}