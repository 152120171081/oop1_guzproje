#include "Point.h"
//Kerem Ko�
//152120181089

//constructor
Point::Point(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;

};
double Point::getX() const
{
	return x;
}
double Point::getY() const 
{
	return y;
}
double Point::getZ() const 
{
	return z;
}
void Point::setX(double x) 
{
	this->x = x;
}
void Point::setY(double y) 
{
	this->y = y;
}
void Point::setZ(double z) 
{
	this->z = z;
}

bool Point::operator==(const Point& p)
{
	return (x == p.x && y == p.y && z == p.z);
}

double Point::distance(const Point& p) const 
{
	return sqrt(pow((p.x - x), 2) + pow((p.y - y), 2) + pow((p.z - z), 2));
}
