#include "PointCloudRecorder.h"
//Kerem Ko�
//152120181089
//08:12.2019
//OOP-1 proje-1

PointCloudRecorder::PointCloudRecorder(string fileName) {
	this->fileName = fileName;
}

string PointCloudRecorder::getFileName() const {
	return fileName;
}

void PointCloudRecorder::setFileName(string fileName) {
	this->fileName = fileName;
}

bool PointCloudRecorder::save(const PointCloud& pc) {
	fstream myFile(fileName);
	vector<Point*>p = pc.getPoints();
	if (myFile.is_open()) {

		for (int i = 0; i < pc.getPointNumber(); i++)
		{
			myFile << p[i]->getX() << " " << p[i]->getY() << " " << p[i]->getZ() << endl;
		}
		return true;
	}
	return false;

}