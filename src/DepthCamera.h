#pragma once
#include "PointCloudGenerator.h"
#include <iostream>
#include<fstream>
#include<sstream>
#include<vector>

//Kerem Ko�
//152120181089
//08:12.2019
//OOP-1 proje-1


class DepthCamera:public PointCloudGenerator
{
private:
	string fileName;
public:
	/// <summary>Constructer with a paremater. </summary>
	/// <param name="fileName"> String value to set private fileName.</param>
	/// <param name="ang[]"> Double array value to set to pointCloudGenerator's Transform's object.</param>
	/// <param name="tr[]"> Double array value to set to pointCloudGenerator's Transform's object.</param>
	DepthCamera(string fileName, double ang[], double tr[]);
	/// <summary>Constructer with a paremater. </summary>
	/// <param name="fileName"> String value to set private fileName.</param>
	/// <param name="ang[]"> Eigen::Vector3d value to set to pointCloudGenerator's Transform's object.</param>
	/// <param name="tr[]"> Eigen::Vector3d value to set to pointCloudGenerator's Transform's object.</param>
	DepthCamera(string fileName, Eigen::Vector3d ang, Eigen::Vector3d tr);

	/// <summary> To get fileName.</summary>
	/// <returns> String fileName value.</returns>
	string getFileName() const;

	/// <summary>To set fileName with new value. </summary>
	/// <param name="fileName"> String value to set private fileName.</param>
	void setFileName(string fileName);

	/// <summary> Read the file and create the PointCloud.</summary>
	/// <returns> Rerturn the new PointCloud.</returns>
	PointCloud capture(); 

	/// <summary> Read the file and create the PointCloud then filter them set on the new space.</summary>
	/// <returns> Rerturn the new PointCloud.</returns>
	PointCloud captureFor();
};

