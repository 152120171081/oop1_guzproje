/*
* @file PointCloud.h
* @author Sezer Demir DEDEK 152120171081
* @date 08/12/2019 Pazar
**/
#include "RadiusOutlierFilter.h"

RadiusOutlierFilter::RadiusOutlierFilter(double radius)
{
	this->radius = radius;
}


RadiusOutlierFilter::~RadiusOutlierFilter()
{
}

double RadiusOutlierFilter::getRadius() const
{
	return radius;
}
void RadiusOutlierFilter::setRadius(double radius)
{
	this->radius = radius;
}
void RadiusOutlierFilter::filter(PointCloud& pc) const
{
	int pointNumber = pc.getPointNumber();
	bool check=false;
	int counter = 0;
	Point *newPoint = new Point[pointNumber]();
	vector<Point*> temp = pc.getPoints();
	for (int i = 0; i < pointNumber; i++)
	{
		for (int j = 0; j < pointNumber; j++)
		{
			if (i != j)
			{
				if (temp[i]->distance(*(temp[j]))<=radius)
				{
					newPoint[counter]=*(temp[i]);
					counter++;
					break;
				}
			}
		}
	}	
	PointCloud *newPC=new PointCloud(counter); 
	
	for (int i = 0; i < counter; i++)
	{
		Point * temp = &newPoint[i];
		newPC->setPoint(temp);
	}
	pc = *newPC;
}